name = input("이름을 입력하시오: ")
print(name, "님 반갑습니다.")

occ = int(input("직업 설명을 보고 싶으시다면 1번 / 직업 선택을 하고싶다면 2번 : "))
if occ == 1:
    Character_dic = {'1.전사':'근거리 딜러이며 주무기가 칼이다.', '2.마법사':'원거리 딜러 및 서포터이며 주무기가 지팡이다.', '3.궁수':'원거리 딜러이며 주무기가 활이다.'}
    print(Character_dic)
    choose = int(input("원하는 직업을 고르세요 -> 1.전사 2.마법사 3.궁수 : "))
if occ == 2:
    choose = int(input("원하는 직업을 고르세요 -> 1.전사 2.마법사 3.궁수 : "))

if choose == 1:
    print(name, "님 직업 전사(HP 100)")
if choose == 2:
    print(name, "님 직업 마법사(HP 100)")
if choose == 3:
    print(name, "님 직업 궁수(HP 100)")

print("이제 팩맨 게임을 시작합니다.")

item_list = ["1.공격력 증가 물약", "2.공격속도 증가 물약", "3.마법 공격력 증가 물약"]
item1 = "1.공격력 증가 물약"
item2 = "2.공격속도 증가 물약"
item3 = "3.마법 공격력 증가 물약"

for i in range(1, 6):
    print(i, "유령이 나탔났다")
    print("1.도망간다 2.아이템쓴다 3.싸운다")
    number = int(input("숫자를 입력하세요: "))
    # 유저가 입력한 글자를 출력
    # print("유저 입력값:", number)
    # 만약에 유저가 입력한 글자가 1이면 도망
    if number == 1:
      print("도망간다")
    # 만약에 유저가 입력한 글자가 2면 아이템
    if number == 2:
      print(item_list[0])
      print(item_list[1])
      print(item_list[2])
    
      item_use = int(input("사용할 아이템을 고르세요 : "))
      if item_use == 1:
        print("공격력이 일시적으로 증가합니다.")
      if item_use == 2:
        print("공격속도가 일시적으로 증가합니다.")
      if item_use == 3:
        print("마법 공격력이 일시적으로 증가합니다.")
      
    # 만약에 유저가 입력한 글자가 3이면 싸운다
    if number == 3:
      print("싸운다")
      from random import randint

      for i in range(1,3):
          print("1.가위 2.바위 3.보")

          computer = randint(1,3)
          user = int(input("선택: "))
          
          if user == 1:
              if computer == 1:
                  print(name, "가위, 유령: 가위")
                  print("비겼다!!")
              elif computer == 2:
                  print(name, "가위, 유령: 바위")
                  print("유령에게 졌다!!")
              else:
                  print(name, "가위, 유령: 보")
                  print("유령에게 이겼다!!")
          
          elif user == 2:
              if computer == 1:
                  print(name, "바위, 유령: 가위")
                  print("유령에게 이겼다!!")
              elif computer == 2:
                  print(name, "바위, 유령: 바위")
                  print("비겼다!!")
              else:
                  print(name, "바위, 유령: 보")
                  print("유령에게 졌다!!")

          else:
              if computer == 1:
                  print(name, "보, 유령: 가위")
                  print("유령에게 이겼다!!")
              elif computer == 2:
                  print(name, "보, 유령: 바위")
                  print("유령에게 졌다!!")
              else:
                  print(name, "보, 유령: 보")
                  print("비겼다!!")
